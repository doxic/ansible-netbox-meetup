# Ansible Netbox Meetup

## Description
[Meetup Slides](https://docs.google.com/presentation/d/e/2PACX-1vRB54TD-WLD2x6NUf7V-gBqqOjt03qcsEAJ0GsFdp43H33eOcALitqTYE7K7IPhGfDtu7ThRmQH4Hhz/pub?start=false&loop=false&delayms=3000)

## Installation

- [Downloads - Git](https://git-scm.com/downloads)
- [Downloads - Oracle VM VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- [Downloads - Vagrant by HashiCorp](https://www.vagrantup.com/downloads)

Create virtual python environment
```bash
[ -d .venv ] || python3 -m venv .venv
source .venv/bin/activate
```

Install requirements
```bash
pip install -r requirements.txt
```

## Demo

```bash
vagrant up
vagrant up --provision
ansible-playbook netbox_populate.yml
ansible-inventory -i netbox_inventory.yml --graph
ansible-playbook netbox_create_context.yml

```

## Cheatsheet

### Vagrant

creates and configures guest machines

```plaintext
vagrant up
```

SSH into a running Vagrant machine and give you access to a shell

```plaintext
vagrant ssh
```

stops the running machine Vagrant is managing and destroys all resources that were created during the machine creation process

```plaintext
vagrant destroy
```

## Release History

- 0.0.2
  - The first working release
  - ADD: README
  - CHANGE: Outsourced provisioner in Vagrantfile
- 0.0.1
  - Initial release
  - Work in progress

## Links

- [Ansible - Short Introduction | Vagrant by HashiCorp](https://www.vagrantup.com/docs/provisioning/ansible_intro)
- [Ansible Constructed Inventory | The NTC Mag](http://blog.networktocode.com/post/ansible-constructed-inventory/)
