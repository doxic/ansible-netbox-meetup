# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "centos/7"

  config.vm.provider :virtualbox do |v|
    v.memory = 512
    v.cpus = 1
    v.linked_clone = true
  end

  # ensure that all Vagrant machines will use the same SSH key pair
  config.ssh.insert_key = false

  boxes = [
    { :name => "nb1", :ip => "10.0.10.10" }
  ]

  ansible_groups = {
  "nb" => [
    "nb1"
  ],
  "netbox" => [
    "nb1"
  ]
}

  # Provision each of the VMs.
  boxes.each_with_index do |opts, index|
    config.vm.define opts[:name] do |config|
      config.vm.hostname = opts[:name]
      config.vm.network :private_network, ip: opts[:ip]

      # Provision all the VMs in parallel using Ansible after last VM is up.
      if index == boxes.size-1
        config.vm.provision "ansible" do |ansible|
          ansible.compatibility_mode = "2.0"
          ansible.playbook = "provision.yml"
          ansible.limit = "all"
          ansible.become = true
          ansible.groups = ansible_groups
        end
      end
    end
  end

end