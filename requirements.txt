# Install pip requirements: pip install -r requirements.txt
# Output to file: pip freeze >> requirements.txt
# Upgrade pip: python -m pip install --upgrade pip

# ansible==3.4.0
# ansible-base==2.10.11
ansible
python-vagrant
pynetbox
netaddr